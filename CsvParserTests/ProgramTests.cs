// Zein Hajj-Ali
// 23-4-2020

using Microsoft.VisualStudio.TestTools.UnitTesting;
using CsvParser;
using System.Collections.Generic;

namespace CsvParserTests
{
    [TestClass]
    public class ProgramTests
    {

        [TestMethod]
        public void LoadCsv_validFilename()
        {
            Parser parser = new Parser();
            string filename = "data.csv";
            CsvData loadedData = parser.LoadCsv(filename);

            //CsvData header and data must not be null
            Assert.IsNotNull(loadedData);
        }

        [TestMethod]
        public void LoadCsv_invalidFilename()
        {
            Parser parser = new Parser();
            string filename = "doesn't exist.csv";

            Assert.ThrowsException<System.IO.FileNotFoundException>(() => parser.LoadCsv(filename));
        }

        [TestMethod]
        public void getTotals_validCsvDataInput()
        {
            Parser parser = new Parser();
            string filename = "data.csv";
            CsvData loadedData = parser.LoadCsv(filename);
            List<ProductData> testData = parser.GetTotals(loadedData);
            Assert.IsInstanceOfType(testData, typeof(List<ProductData>));
            
        }

        [TestMethod]
        public void GetPopular_validListInput()
        {
            Parser parser = new Parser();
            string filename = "data.csv";
            CsvData loadedData = parser.LoadCsv(filename);
            List<ProductData> testData = parser.GetTotals(loadedData);
            ProductData mostPopular = parser.GetPopular(testData);
            Assert.IsInstanceOfType(mostPopular,typeof(ProductData));
        }

        [TestMethod]
        public void GetTotalRevenue_validInput()
        {
            Parser parser = new Parser();
            string filename = "data.csv";
            CsvData loadedData = parser.LoadCsv(filename);
            List<ProductData> testData = parser.GetTotals(loadedData);
            float totalRev = parser.GetTotalRevenue(testData);
            Assert.IsTrue(totalRev > testData[0].totalRevenue + testData[1].totalRevenue);
        }

        [TestMethod]
        public void GetRevenueByMonth_validInput()
        {
            Parser parser = new Parser();
            string filename = "data.csv";
            CsvData loadedData = parser.LoadCsv(filename);
            List<DatedRevenue> testDated = parser.GetMonthlyRevenue(loadedData);
            Assert.IsInstanceOfType(testDated, typeof(List<DatedRevenue>));
        }

        [TestMethod]
        public void GetHighestMonthlyRevenue_validInput()
        {
            Parser parser = new Parser();
            string filename = "data.csv";
            CsvData loadedData = parser.LoadCsv(filename);
            List<DatedRevenue> testDated = parser.GetMonthlyRevenue(loadedData);
            DatedRevenue highestRevenue = parser.GetHighestMonthlyRevenue(testDated);
            Assert.IsTrue(highestRevenue.totalRevenue >= testDated[0].totalRevenue);
            Assert.IsTrue(highestRevenue.totalRevenue >= testDated[testDated.Count - 1].totalRevenue);
        }
    }
}
