﻿// Zein Hajj-Ali
// 23-4-2020

using System;
using System.IO;
using System.Collections.Generic;

namespace CsvParser
{
    public class CsvData
    {
        public string[] header;
        public string[][] data;
    }

    public class DatedRevenue
    {
        public int Month { get; set; }
        public float totalRevenue { get; set; }

        public override string ToString()
        {
            return "Month " + Month + " had a total revenue of " + totalRevenue;
        }

        public override bool Equals(object obj)
        {
            if (obj is DatedRevenue == false)
            {
                return false;
            }
            return Equals((DatedRevenue)obj);
        }

        public bool Equals(DatedRevenue other)
        {
            if (this.Month == other.Month)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class ProductData
    {
        public int ProductId { get; set; }
        public int totalSold { get; set; }
        public float totalRevenue { get; set; }

        public override string ToString()
        {
            return "Product ID: " + ProductId + "    Total Sold: " + totalSold + "    Total Revenue: " + totalRevenue;
        }

        public override bool Equals(object obj)
        {
            if (obj is ProductData == false)
            {
                return false;
            }
            return Equals((ProductData)obj);
        }
        public bool Equals(ProductData other)
        {
            if (this.ProductId == other.ProductId)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class Parser
    {
        public static void Main(string[] args)
        {
            List<string> toBeWritten = new List<string>();
            Parser parser = new Parser();
            CsvData loadedData = parser.LoadCsv("data.csv");
            List<ProductData> totals = parser.GetTotals(loadedData);
            foreach (ProductData element in totals)
            {
                toBeWritten.Add(element.ToString());
            }

            toBeWritten.Add("\nMost popular product (by total amount sold): ");
            toBeWritten.Add(parser.GetPopular(totals).ToString());

            toBeWritten.Add("\nThe total revenue of all products is: ");
            toBeWritten.Add(parser.GetTotalRevenue(totals).ToString());

            toBeWritten.Add("\nThe month with the highest revenue: ");
            List<DatedRevenue> totalsByMonth = parser.GetMonthlyRevenue(loadedData);
            toBeWritten.Add(parser.GetHighestMonthlyRevenue(totalsByMonth).ToString());

            string path = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName;
            string filename = Path.Combine(path, "results.txt");

            using (StreamWriter outputFile = new StreamWriter(filename))
            {
                foreach (string line in toBeWritten)
                {
                    outputFile.WriteLine(line);
                }
            }
            Console.WriteLine("Data was written to " + filename);
        }

        public CsvData LoadCsv(string filename) 
        {
            CsvData loadedData = new CsvData();
            string[] arr = System.IO.File.ReadAllLines(filename);
            
            loadedData.header = arr[0].Split(',');
            
            loadedData.data = new string[arr.Length - 1][];
            for (int j = 1; j < arr.Length; j++)
            {
                loadedData.data[j - 1] = arr[j].Split(',');
            }
            return loadedData;
        }

        public List<ProductData> GetTotals(CsvData loadedData)
        {
            List<ProductData> totals = new List<ProductData>();
            ProductData element;
            for (int j = 0; j < loadedData.data.Length; j++)
            {
                element = new ProductData { ProductId = int.Parse(loadedData.data[j][0]) };
                if (totals.Contains(element))
                {
                    element = totals.Find(x => x.ProductId == int.Parse(loadedData.data[j][0]));
                    element.totalSold += int.Parse(loadedData.data[j][1]);
                    element.totalRevenue += float.Parse(loadedData.data[j][1]) * float.Parse(loadedData.data[j][2]);
                }
                else
                {
                    totals.Add(element);
                    element.totalSold = int.Parse(loadedData.data[j][1]);
                    element.totalRevenue = float.Parse(loadedData.data[j][1]) * float.Parse(loadedData.data[j][2]);
                }
            }

            return totals;
        }

        public ProductData GetPopular(List<ProductData> totals)
        {
            if (totals.Count == 0)
            {
                throw new InvalidOperationException("Empty list");
            }

            ProductData mostPopular = totals[0];
            foreach (ProductData element in totals)
            {
                if (element.totalSold > mostPopular.totalSold)
                {
                    mostPopular = element;
                }
            }

            return mostPopular;
        }

        public float GetTotalRevenue(List<ProductData> totals)
        {
            if (totals.Count == 0)
            {
                throw new InvalidOperationException("Empty list");
            }

            float totalRevenue = 0;
            foreach (ProductData element in totals)
            {
                totalRevenue += element.totalRevenue;
            }

            return totalRevenue;
        }

        public List<DatedRevenue> GetMonthlyRevenue(CsvData loadedData)
        {
            List<DatedRevenue> totalsByMonth = new List<DatedRevenue>();
            DatedRevenue element;
            for (int j = 0; j < loadedData.data.Length; j++)
            {
                element = new DatedRevenue { Month = int.Parse(loadedData.data[j][3].Split('-')[1]) };
                if (totalsByMonth.Contains(element))
                {
                    element = totalsByMonth.Find(x => x.Month == int.Parse(loadedData.data[j][3].Split('-')[1]));
                    element.totalRevenue += float.Parse(loadedData.data[j][1]) * float.Parse(loadedData.data[j][2]);
                }
                else
                {
                    totalsByMonth.Add(element);
                    element.totalRevenue = float.Parse(loadedData.data[j][1]) * float.Parse(loadedData.data[j][2]);
                }
            }

            return totalsByMonth;
        }

        public DatedRevenue GetHighestMonthlyRevenue(List<DatedRevenue> totalsByMonth)
        {
            if (totalsByMonth.Count == 0)
            {
                throw new InvalidOperationException("Empty list");
            }

            DatedRevenue highestRevenue = totalsByMonth[0];
            foreach (DatedRevenue element in totalsByMonth)
            {
                if (element.totalRevenue > highestRevenue.totalRevenue)
                {
                    highestRevenue = element;
                }
            }

            return highestRevenue;
        }
    }
}
